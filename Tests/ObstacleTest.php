<?php
/**
 * Created by PhpStorm.
 * User: Mrk
 * Date: 2017-12-20
 * Time: 14:21
 */


require_once dirname(__DIR__) . '/Classes/Obstacle.php';
use PHPUnit\Framework\TestCase;
use Classes\Obstacle;

class ObstacleTest extends TestCase
{
    protected $_obstacle;

    public function setUp() {
        $this->_obstacle = new Obstacle(array(1,1));
    }

    public function test__construct()
    {
        $this->assertEquals( new Obstacle(array(1,1)), $this->_obstacle);
    }
}
