<?php
/**
 * Created by PhpStorm.
 * User: Mrk
 * Date: 2017-12-20
 * Time: 10:45
 */

require_once dirname(__DIR__) . '/Classes/Robot.php';

use PHPUnit\Framework\TestCase;
use Classes\Robot;
class RobotTest extends TestCase {
    protected $_robot;

    protected function setUp() {
        $this->_robot = new Robot(array(1,1));
    }

    public function test__construct(){
        $this->assertEquals( new Robot(array(1,1),"North"), $this->_robot);
    }

    public function testGetDirection(){
        $this->assertEquals("North",$this->_robot->getDirection());
    }

    public function testSetDirection(){
        $this->_robot->setDirection("South");
        $this->assertEquals("South",$this->_robot->getDirection());
    }

    public function testMoveForward()
    {
        $this->_robot->move('f');
        $this->assertEquals(array(1,0),$this->_robot->getPosition());
    }
    public function testMoveBackward()
    {
        $this->_robot->move('b');
        $this->assertEquals(array(1,2),$this->_robot->getPosition());
    }
    public function testMoveLeft()
    {
        $this->_robot->move('l');
        $this->assertEquals("West",$this->_robot->getDirection());
    }
    public function testMoveRight()
    {
        $this->_robot->move('r');
        $this->assertEquals("East",$this->_robot->getDirection());
    }
}
