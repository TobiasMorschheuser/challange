<?php
/**
 * Created by PhpStorm.
 * User: Mrk
 * Date: 2018-01-04
 * Time: 12:08
 */
require_once dirname(__DIR__) . '/Classes/Obstacle.php';
require_once dirname(__DIR__) . '/Classes/Game.php';
require_once dirname(__DIR__) . '/Classes/Robot.php';

use Classes\Game;
use Classes\Robot;
use Classes\Obstacle;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    protected $_game;

    public function testAddObstacle()
    {
        $this->_game = new Game(array(1,1),array(100,100),0);
        $this->_game->addObstacle(2,1);
        $this->assertEquals(new Obstacle(array(2,1)),$this->_game->gridAt(2,1));
    }

    public function testCreateObstacle()
    {
        $this->_game = new Game(array(1,1),array(100,100),0);
        $this->_game->addObstacle(5,12);
        $this->_game->addObstacle(12,19);
        $this->_game->addObstacle(19,24);
        $this->_game->addObstacle(24,36);
        $this->assertEquals(new Obstacle(array(5,12)),$this->_game->getObstacles()[0]);
        $this->assertEquals(new Obstacle(array(12,19)),$this->_game->getObstacles()[1]);
        $this->assertEquals(new Obstacle(array(19,24)),$this->_game->getObstacles()[2]);
        $this->assertEquals(new Obstacle(array(24,36)),$this->_game->getObstacles()[3]);
    }
    /*
     * - The robot is on a 100×100 grid at location (0, 0) and facing SOUTH. The robot is given the commands “fflff” and should end up at (2, 2)
    */
    public function testMove()
    {
        $this->_game = new Game(array(0,0),array(100,100),0);
        $this->_game->getRobot()->setDirection("South");
        $this->_game->move("fflff");
        $this->assertEquals(new Robot(array(2, 2),"East"),$this->_game->getRobot());
    }

    /*
    * - The robot is on a 50×50 grid at location (1, 1) and facing NORTH. The robot is given the commands “fflff” and should end up at (1, 0)
    */
    public function testMove2()
    {
        $this->_game = new Game(array(1,1),array(100,100),0);
        $this->_game->getRobot()->setDirection("North");
        $this->_game->move("fflff");
        $this->assertEquals(new Robot(array(1, 0)),$this->_game->getRobot());
    }

    /*
     * - The robot is on a 100×100 grid at location (50, 50) and facing NORTH. The robot is given the commands “fflffrbb” but there is an obstacle at (48, 50) and should end up at (48, 49)
     */
    public function testMove3()
    {
        $this->_game = new Game(array(50,50),array(100,100),0);
        $this->_game->getRobot()->setDirection("North");
        $this->_game->addObstacle(48, 50);
        $this->_game->move("fflffrbb");
        $this->assertEquals(new Robot(array(48,49)),$this->_game->getRobot());
    }
}
