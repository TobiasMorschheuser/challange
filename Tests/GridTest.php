<?php
/**
 * Created by PhpStorm.
 * User: Mrk
 * Date: 2017-12-25
 * Time: 16:01
 */

require_once dirname(__DIR__) . '/Classes/Grid.php';

use PHPUnit\Framework\TestCase;
use Classes\Grid;



class GridTest extends TestCase
{
    protected $_grid;

    public function setUp() {
        $this->_grid = new Grid();
    }

    public function test__construct()
    {
        $this->assertEquals( new Grid(), $this->_grid);
    }

}
