<?php

namespace Classes;

class Grid
{
    private $_size,
            $_grid;

    public function __construct(int $x = 100, int $y = 100)
    {
        $this->_size = array($x,$y);
        $this->init();
    }

    private function init(){
        $this->_grid = array(array());
        for ($i = 0; $i <= $this->_size[0]; $i++){
            for ($j = 0; $j <= $this->_size[1]; $j++){
                $this->_grid[$i][$j] = null;
            }
        }
    }

    public function getSize(): array
    {
        return $this->_size;
    }
    public function getSizeX(): int
    {
        return $this->_size[0];
    }
    public function getSizeY(): int
    {
        return $this->_size[1];
    }
    public function getGrid(): array
    {
        return $this->_grid;
    }

    public function at($x,$y){
        if ($x < 0) {
            $x = 0;
        }
        if ($y < 0) {
            $y = 0;
        }
        if ($x > $this->getSizeX()) {
            $x = $this->getSizeX();
        }
        if ($y > $this->getSizeY()) {
            $y = $this->getSizeY();
        }
       return $this->_grid[$x][$y];
    }

    public function removeAt($x,$y): void{
         $this->_grid[$x][$y] = null;
    }
    public function add($x,$y,$obj): void{
        if ($x >= 0 && $x < $this->_size[0]){
            if ($y >= 0 && $y < $this->_size[1]){
                $this->_grid[$x][$y] = $obj;
            }
        }
    }
}

