<?php
/**
 * Created by PhpStorm.
 * User: Mrk
 * Date: 2017-12-20
 * Time: 11:19
 */

namespace Classes;
require_once 'Element.php';

class Robot extends Element
{
    private const AVAILABLE_DIRECTIONS = array("North", "West", "South", "East");
    private $_direction;

    public function __construct($pos = array(), String $dir = self::AVAILABLE_DIRECTIONS[0])
    {
        $this->setPosition($pos);
        $this->_direction = $dir;
    }
    public function getDirection(): string
    {
        return (string)$this->_direction;
    }
    public function setDirection(string $dir): void
    {
        if (in_array($dir, self::AVAILABLE_DIRECTIONS)) {
            $this->_direction = $dir;
        } else {
            $this->_direction = self::AVAILABLE_DIRECTIONS[0];
        }
    }
    public function move(String $dir): void
    {
        $dir = strtolower($dir);
        switch ($dir) {
            case 'f':
                $this->forward();
            break;
            case 'b':
                $this->backward();
            break;
            case 'r':
                $this->right();
            break;
            case 'l':
                $this->left();
            break;
        }
    }

    private function forward(): void
    {
        switch ($this->_direction) {
            case 'North':
                $this->setY($this->getY() - 1);
            break;
            case 'West':
                $this->setX($this->getX() - 1);
            break;
            case 'South':
                $this->setY($this->getY() + 1);
            break;
            case 'East':
                $this->setX($this->getX() + 1);
            break;
        }
    }
    private function backward(): void
    {
        switch ($this->_direction) {
            case 'North':
                $this->setY($this->getY() + 1);
            break;
            case 'West':
                $this->setX($this->getX() + 1);
            break;
            case 'South':
                $this->setY($this->getY() - 1);
            break;
            case 'East':
                $this->setX($this->getX() - 1);
            break;
        }
    }
    private function left(): void
    {
        switch ($this->_direction) {
            case 'North':
                $this->_direction = "West";
            break;
            case 'East':
                $this->_direction = "North";
            break;
            case 'South':
                $this->_direction = "East";
            break;
            case 'West':
                $this->_direction = "South";
            break;
        }
    }
    private function right(): void
    {
        switch ($this->_direction) {
            case 'North':
                $this->_direction = "East";
            break;
            case 'East':
                $this->_direction = "South";
            break;
            case 'South':
                $this->_direction = "West";
            break;
            case 'West':
                $this->_direction = "North";
            break;
        }
    }

    protected function toStringSpec(): String
    {
        return "Direction: " . $this->_direction . "\n";
    }


}/* Class */
