<?php
/**
 * Created by PhpStorm.
 * User: Mrk
 * Date: 2017-12-20
 * Time: 11:19
 */

namespace Classes;

abstract class Element
{
    public $_position;
    protected const X_POS = 0,
                    Y_POS = 1;

    public function getX():int {
        return $this->_position[self::X_POS];
    }
    public function getY():int {
        return $this->_position[self::Y_POS];
    }
    public function getPosition():array {
        return $this->_position;
    }

    public function setX(int $x): void {
        $this->_position[self::X_POS] = $x;
    }
    public function setY(int $y): void {
        $this->_position[self::Y_POS] = $y;
    }
    public function setPosition($pos = array()): void {
        if (count($pos) == 2) {
            $this->_position = $pos;
        }
    }

    public function toString(): String {
        return "X: " . $this->getX() . " Y:" . $this->getY() . "\n". $this->toStringSpec();
    }

    abstract protected function toStringSpec():String;

} /* CLASS */