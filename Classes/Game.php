<?php
/**
 * Created by PhpStorm.
 * User: Mrk
 * Date: 2017-12-26
 * Time: 17:04
 */

namespace Classes;

require_once dirname(__DIR__) . '/Classes/Grid.php';
require_once dirname(__DIR__) . '/Classes/Robot.php';
require_once dirname(__DIR__) . '/Classes/Obstacle.php';

class Game {
    private $_robot,
            $_grid,
            $_obstacles;

    public function __construct($robotPos = array(),$sizeGrid = array(),int $nrOfObstacles)
    {
        $this->_robot = new Robot(array($robotPos[0],$robotPos[1]));
        $this->_grid = new Grid($sizeGrid[0],$sizeGrid[1]);
        $this->_obstacles = array();

        for ($i = 0; $i < $nrOfObstacles; $i++){
            $this->addObstacle(rand(0,$sizeGrid[0]),rand(0,$sizeGrid[1]));
        }
    }

    public function addObstacle(int $x,int $y): void {
        $this->addItem($this->createObstacle($x,$y));
    }
    public function addItem(Element $obj): void {
        if (is_subclass_of($obj,'Classes\Element')){
            $this->_grid->add($obj->getX(),$obj->getY(),$obj);
        }
    }

    public function move(String $str): bool {
        $move = true;
        for ($i = 0; $i < strlen($str) && $move; $i++) {
            if ($str[$i] == 'f' && (!$this->checkForObstaclesForward() || !$this->cehckSizeForward())) {
                $move = false;
            } else if($str[$i] == 'b' && (!$this->checkForObstaclesBackward() || !$this->cehckSizeBackward())) {
                $move = false;
            }
            if ($move) {
                // remove old position
                $this->_grid->removeAt($this->_robot->getX(),$this->_robot->getY());

                $this->_robot->move($str[$i]);
                //update new position
                $this->addItem($this->_robot);
            }
        }
        return $move;
    }

    // Kan returnera Obstacle, Robot eller Null
    public function gridAt(int $x,int $y): Element {
        return $this->_grid->at($x,$y);
    }

    public function getObstacles(): array
    {
        return $this->_obstacles;
    }
    public function getRobot(): Robot
    {
        return $this->_robot;
    }

    // lägger till sist, returnerar tillbaka objektet.
    private function createObstacle(int $x, int $y): Obstacle {
        $pos = sizeof($this->_obstacles);
        $this->_obstacles[$pos] = new Obstacle(array($x,$y));
        return $this->_obstacles[$pos];
    }

    // Kollar efter Obstacles frammåt och bakåt
    private function checkForObstaclesForward(): bool {
       $return = true;
        switch ($this->_robot->getDirection()) {
            case 'North':
                if ($this->_grid->at($this->_robot->getX() ,$this->_robot->getY() - 1) instanceof Obstacle) {
                    $return = false;
                }
            break;
            case 'West':
                if ($this->_grid->at($this->_robot->getX() - 1, $this->_robot->getY()) instanceof Obstacle) {
                    $return = false;
                }
            break;
            case 'South':
                if ($this->_grid->at($this->_robot->getX(),$this->_robot->getY() + 1) instanceof Obstacle) {
                    $return = false;
                }
            break;
            case 'East':
                if ($this->_grid->at($this->_robot->getX() + 1, $this->_robot->getY()) instanceof Obstacle) {
                    $return = false;
                }
            break;
        }/* switch */
        return $return;
    }
    private function checkForObstaclesBackward(): bool {
        $return = true;
        switch ($this->_robot->getDirection()) {
            case 'North':
                if ($this->_grid->at($this->_robot->getX(), $this->_robot->getY() + 1) instanceof Obstacle) {
                    $return = false;
                }
            break;
            case 'West':
                if ($this->_grid->at($this->_robot->getX() + 1,$this->_robot->getY()) instanceof Obstacle) {
                    $return = false;
                }
            break;
            case 'South':
                if ($this->_grid->at($this->_robot->getX() ,$this->_robot->getY() - 1) instanceof Obstacle) {
                    $return = false;
                }
            break;
            case 'East':
                if ($this->_grid->at($this->_robot->getX() - 1,$this->_robot->getY()) instanceof Obstacle) {
                    $return = false;
                }
            break;
        }/* switch */
        return $return;
    }

    private function cehckSizeForward(): bool{
        $return = true;
        switch ($this->_robot->getDirection()) {
            case 'North':
                if ($this->_robot->getY() - 1 < 0) {
                    $return = false;
                }
            break;
            case 'West':
                if ($this->_robot->getX() - 1 < 0) {
                    $return = false;
                }
            break;
            case 'South':
                if ($this->_robot->getY() + 1 > $this->_grid->getSizeY()) {
                    $return = false;
                }
            break;
            case 'East':
                if ($this->_robot->getX() + 1 > $this->_grid->getSizeX()) {
                    $return =false;
                }
            break;
        }/* switch */
        return $return;
    }
    private function cehckSizeBackward(): bool{
        $return = true;
        switch ($this->_robot->getDirection()) {
            case 'North':
                if ($this->_robot->getY() + 1 > $this->_grid->getSizeY()) {
                    $return = false;
                }
            break;
            case 'West':
                if ($this->_robot->getX() + 1 > $this->_grid->getSizeX()) {
                    $return = false;
                }
            break;
            case 'South':
                if ($this->_robot->getY() - 1 < 0) {
                    $return = false;
                }
            break;
            case 'East':
                if ($this->_robot->getX() - 1 < 0) {
                    $return =  false;
                }
            break;
        }/* switch */
        return $return;
    }
}